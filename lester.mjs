import assert from './utils/assert.mjs'
import asyncForEach from './utils/foreach.mjs'
import * as log from './utils/log.mjs'
// import getFiles from './utils/getFiles.mjs'

// getFiles(() => {
//   console.log('Yolo')
// })

const queue = []
const stats = { passed: 0, failed: 0 }

export const test = (title, fn) => {
  queue.push({ title, fn })
}

const run = async () => {
  const ts = Date.now()

  await asyncForEach(queue, async test => {
    await each(test, ts)
  })

  const ms = Date.now() - ts
  log.total(stats.passed, stats.failed)
  log.elapsed(ms)
}

const each = async ({ title, fn }, ts) => {
  try {
    await fn(assert)
    stats.passed += 1
    log.success(title)
  } catch (err) {
    stats.failed += 1
    log.error(title)
  }
}

process.once('beforeExit', () => {
  run()
})

export default test
