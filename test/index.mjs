import test from '../lester.mjs'

test('should pass', is => {
  is.passing()
})

// test('should fail', is => {
//   is.failing()
// })

test('strings are equal', is => {
  is.equal('foo', 'foo')
})

test('ints are equal', is => {
  is.equal(5, 5)
})

test('is true', is => {
  const bool = true
  is.true(bool)
})

test('is false', is => {
  const bool = false
  is.false(bool)
})

test('is bool', is => {
  const bool = false
  is.bool(bool)
})

test('is array', is => {
  const arr = [1, 2, 3]
  is.array(arr)
})

test('is object', is => {
  const obj = { foo: 'foo', bar: 'bar' }
  is.object(obj)
})

test('is date', is => {
  const now = new Date()
  is.date(now)
})

test('is int', is => {
  const int = 10
  is.int(int)
})

test('is url', is => {
  const url = 'https://gitlab.com/imlinus/lester'
  is.url(url)
})

test('is function', is => {
  const fn = () => 'Lester!'
  is.function(fn)
})

test('is empty', is => {
  const arr = []
  is.empty(arr)
})

test('is null', is => {
  const str = null
  is.null(str)
})

test('is undefined', is => {
  const str = undefined
  is.undefined(str)
})

test('is error', is => {
  const err = new Error('This is a error msg')
  is.error(err)
})
