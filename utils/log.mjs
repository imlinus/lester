export const success = (title, elapsed) => {
  console.log('\x1b[32m✔\x1b[0m success: %s', title)
}

export const error = (title, elapsed) => {
  console.log('\x1b[31m✘\x1b[0m error: %s', title)
}

export const total = (passed, failed) => {
  console.log('')

  if (failed === 0) {
    console.log('\x1b[42m\x1b[44m Wohoo! 🎉  \x1b[0m')
    console.log('\x1b[30m\x1b[42m All %s Passed \x1b[0m', passed)
  } else {
    console.log('\x1b[30m\x1b[42m %s passed \x1b[0m\x1b[30m\x1b[41m %s failed \x1b[0m', passed, failed)
  }
}

export const elapsed = ms => {
  console.log('')
  console.log('\x1b[30m\x1b[36m In %sms \x1b[0m', ms)
}
