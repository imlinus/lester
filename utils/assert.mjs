export default {
  passing: () => true,
  failing: () => {
    throw new Error('is failing')
  },

  equal: (match, to) => {
    if (match === to) return true
    else throw new Error(match + ' & ' + to + ' are not equal')
  },

  true: (val) => {
    if (val) return true
    else throw new Error(val + ' is not true')
  },

  false: val => {
    if (!val) return true
    else throw new Error(val + ' is not false')
  },

  bool: bool => {
    if (typeof bool === 'boolean') return true
    else throw new Error(bool + ' is not a boolean')
  },

  int: int => {
    if (Number.isInteger(int)) return true
    else throw new Error(int + ' is not a int')
  },

  date: val => {
    if (val instanceof Date) return true
    else throw new Error(val + ' is not a Date')
  },

  array: arr => {
    if (Array.isArray(arr)) return true
    else throw new Error(arr + ' is not a Array')
  },

  object: obj => {
    if (typeof obj === 'object') return true
    else throw new Error(obj + ' is not a Object')
  },

  promise: prom => {
    if (Promise.resolve(prom) === prom) return true
    else throw new Error(prom + ' is not a Promise')
  },

  function: fn => {
    if (fn instanceof Function) return true
    else throw new Error(fn + ' is not a Function')
  },

  url: url => {
    var regex = /(http|https):\/\/(\w+:{0,1}\w*)?(\S+)(:[0-9]+)?(\/|\/([\w#!:.?+=&%!\-/]))?/

    if (regex.test(url)) return true
    else throw new Error(url + ' is not a URL')
  },

  undefined: val => {
    if (typeof val === 'undefined') return true
    else throw new Error(val + ' is not undefined')
  },

  empty: val => {
    if (!(Object.keys(val) || val).length) return true
    else throw new Error(val + ' is not empty')
  },

  null: val => {
    if (val === null) return true
    else throw new Error(val + ' is not null')
  },

  error: err => {
    if (err instanceof Error) return true
    else throw new Error(err + ' is not Error')
  }
}
