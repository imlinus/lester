import fs from 'fs'
import path from 'path'

const walk = (dir, done) => {
  var results = []
  const regExcludes = [/index\.html/, /js\/lib\.js/, /node_modules/, /.git/]

  fs.readdir(dir, (err, list) => {
    if (err) return done(err)

    var pending = list.length
    if (!pending) return done(null, results)

    list.forEach(file => {
      file = path.join(dir, file)
      var excluded = false

      for (var i = 0; i < regExcludes.length; i++) {
        if (file.match(regExcludes[i])) {
          excluded = true
        }
      }

      // Add if not in regExcludes
      if (excluded === false) {
        results.push(file)

        fs.stat(file, (err, stat) => {
          if (err) throw err

          if (stat && stat.isDirectory()) {
            walk(file, (err, res) => {
              if (err) throw err
              results = results.concat(res)
              if (!--pending) done(null, results)
            })
          } else {
            if (!--pending) done(null, results)
          }
        })
      } else {
        if (!--pending) done(null, results)
      }
    }) // forEach list
  })
}

export default walk
