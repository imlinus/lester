import walk from './walk.mjs'
import match from './match.mjs'
import asyncForEach from './foreach.mjs'

const getFiles = cb => {
  walk('.', (err, results) => {
    if (err) throw err

    asyncForEach(results, async file => {
      await match(file)
    })

    cb()
  })
}

export default getFiles
